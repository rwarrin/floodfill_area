#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <limits.h>

#define Assert(Expression) if(!(Expression)) { *(int *)0 = 0; }

struct memory_arena
{
	void *Base;
	int32_t Size;
	int32_t Used;
};

struct file_result
{
	int32_t FileSize;
	char *FileData;
};

struct grid
{
	int32_t Width;
	int32_t Height;
	struct cell *Cells;
};

struct cell
{
	int32_t X;
	int32_t Y;
	int32_t Visited;
	char Value;
};

struct queue_node
{
	struct cell *Cell;
	struct queue_node *NextNode;
};

struct queue
{
	struct queue_node *Head;
	struct queue_node *Tail;
	int32_t Count;
	int32_t Size;
	struct memory_arena _Storage;
	struct queue_node *_FreeList;
};

static inline void
InitializeArena(struct memory_arena *Arena, int32_t Size)
{
	Arena->Base = (void *)malloc(sizeof(unsigned char) * Size);
	Assert(Arena->Base != NULL);
	Arena->Size = Size;
	Arena->Used = 0;
}

static inline void
DestroyArena(struct memory_arena *Arena)
{
	Assert(Arena != NULL)
	{
		Arena->Size = 0;
		Arena->Used = 0;
		free(Arena->Base);
		Arena->Base = 0;
	}
}

#define PushStruct(Arena, Type) ((Type *)_PushArena(Arena, sizeof(Type)))
static inline void *
_PushArena(struct memory_arena *Arena, int32_t Size)
{
	Assert(Arena->Used + Size <= Arena->Size);

	void *Block = (unsigned char *)Arena->Base + Arena->Used;
	Arena->Used = Arena->Used + Size;

	return(Block);
}

static inline void
InitQueue(struct queue *Queue, int32_t Size)
{
	Assert(Queue != NULL);
	if(Queue)
	{
		Queue->Count = 0;
		Queue->Head = 0;
		Queue->Tail = 0;
		Queue->Size = Size;
		Queue->_FreeList = NULL;
		InitializeArena(&Queue->_Storage, Queue->Size);
	}
}

static inline void
DeleteQueue(struct queue *Queue)
{
	Assert(Queue != NULL);
	if(Queue)
	{
		Queue->Count = 0;
		Queue->Head = 0;
		Queue->Tail = 0;
		Queue->Size = 0;
		Queue->_FreeList = 0;
		DestroyArena(&Queue->_Storage);
	}
}

static inline void
Enqueue(struct queue *Queue, struct cell *Cell)
{
	Assert(Queue != NULL);
	Assert(Queue->Count + 1 < Queue->Size);

	struct queue_node *Node = 0;
	if(Queue->_FreeList != NULL)
	{
		Node = Queue->_FreeList;
		Queue->_FreeList = Queue->_FreeList->NextNode;
	}
	else
	{
		Node = PushStruct(&Queue->_Storage, struct queue_node);
	}

	Node->Cell = Cell;
	Node->NextNode = NULL;

	if(Queue->Head == NULL)
	{
		Queue->Head = Node;
	}

	if(Queue->Tail == NULL)
	{
		Queue->Tail = Node;
	}
	else
	{
		Queue->Tail->NextNode = Node;
		Queue->Tail = Node;
	}

	++Queue->Count;
}

static inline void *
Dequeue(struct queue *Queue)
{
	Assert(Queue != NULL);
	void *Result = 0;

	if(Queue->Head != NULL)
	{
		struct queue_node *Temp = Queue->Head;
		Result = (void *)Queue->Head;
		Queue->Head = Queue->Head->NextNode;
		--Queue->Count;

		Temp->NextNode = Queue->_FreeList;
		Queue->_FreeList = Temp;
	}

	return(Result);
}

static inline void
AddCellToQueueIfNotVisitedAndHasValue(struct queue *Queue, char Value, struct cell *Cell)
{
	Assert(Queue != NULL);

	if(Cell != NULL)
	{
		if((Cell->Value == Value) && (Cell->Visited == 0))
		{
			Enqueue(Queue, Cell);
		}
	}
}

static inline int32_t
IsValidGridPoint(struct grid *Grid, int32_t X, int32_t Y)
{
	int32_t Result = (((X >= 0) && (X < Grid->Width)) &&
					  ((Y >= 0) && (Y < Grid->Height)));
	return(Result);
}

static inline struct cell *
GetGridCellFromCoordinate(struct grid *Grid, int32_t X, int32_t Y)
{
	struct cell *Result = 0;
	if(IsValidGridPoint(Grid, X, Y))
	{
		Result = (Grid->Cells + (Y * Grid->Width) + X);
	}

	return(Result);
}

static int32_t
FloodFill(struct grid *Grid, int32_t Y, int32_t X)
{
	int32_t FillArea = 0;
	struct cell *TargetCell = (Grid->Cells + (Y * Grid->Width) + X);

	struct queue Queue;
	InitQueue(&Queue, (sizeof(struct queue_node) * (Grid->Width * Grid->Height)));
	Enqueue(&Queue, TargetCell);

	struct queue_node *DequeuedNode = 0;
	while((DequeuedNode = (struct queue_node *)Dequeue(&Queue)) != NULL)
	{
		struct cell *Cell = DequeuedNode->Cell;
		if(Cell->Visited == 0)
		{
			Cell->Visited = 1;
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X - 1, Cell->Y - 1));  // Top Left
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X, Cell->Y - 1));  // Top
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X + 1, Cell->Y - 1));  // Top Right
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X + 1, Cell->Y));  // Right
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X + 1, Cell->Y + 1));  // Bottom Right
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X, Cell->Y + 1));  // Bottom
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X - 1, Cell->Y + 1));  // Bottom Left
			AddCellToQueueIfNotVisitedAndHasValue(&Queue, TargetCell->Value, GetGridCellFromCoordinate(Grid, Cell->X - 1, Cell->Y));  // Left
			++FillArea;
		}
	}

	DeleteQueue(&Queue);
	return(FillArea);
}

static struct file_result
ReadEntireFileIntoMemory(char *FileName)
{
	struct file_result Result;
	Result.FileSize = 0;

	FILE *File = fopen(FileName, "rb");
	if(File != 0)
	{
		fseek(File, 0, SEEK_END);
		Result.FileSize = ftell(File);
		fseek(File, 0, SEEK_SET);

		Result.FileData = (char *)malloc(sizeof(char) * Result.FileSize + 1);
		int32_t BytesRead = fread(Result.FileData, sizeof(char), Result.FileSize, File);
		Result.FileData[BytesRead] = 0;
	}
	else
	{
		fprintf(stderr, "Failed to open file '%s'\n", FileName);
	}
	fclose(File);

	return(Result);
}

static void
DestroyFileResult(struct file_result *FileResult)
{
	if(FileResult != 0)
	{
		free(FileResult->FileData);
	}
}

static struct grid
LoadGridFromFile(struct file_result File)
{
	struct grid Grid;
	Grid.Width = 0;
	Grid.Height = 0;
	Grid.Cells = 0;

	int32_t WidthSet = 0;
	for(char *At = File.FileData; *At != 0; ++At)
	{
		if(*At == '\n')
		{
			WidthSet = 1;
			++Grid.Height;
		}

		if(WidthSet == 0)
		{
			++Grid.Width;
		}
	}

	Grid.Cells = (struct cell *)malloc(sizeof(struct cell) * (Grid.Width * Grid.Height));
	struct cell *Cell = Grid.Cells;
	int32_t CellsSetCount = 0;
	for(char *At = File.FileData; *At != 0; ++At)
	{
		if((*At != '\n') && (*At != 0))
		{
			Cell->X = CellsSetCount % Grid.Width;
			Cell->Y = CellsSetCount / Grid.Width;
			Cell->Visited = 0;
			Cell->Value = *At;
			++CellsSetCount;
			++Cell;
		}
	}

	return(Grid);
}

static void
FreeGrid(struct grid *Grid)
{
	if(Grid != 0)
	{
		Grid->Width = 0;
		Grid->Height = 0;
		free(Grid->Cells);
		Grid->Cells = 0;
	}
}

int
main(int ArgCount, char **Args)
{
	if(ArgCount != 2)
	{
		printf("Usage %s <filename>\n", Args[0]);
		return 1;
	}

	struct file_result File = ReadEntireFileIntoMemory(Args[1]);
	struct grid Grid = LoadGridFromFile(File);

	int32_t MaxSize = 0;
	struct cell *MaxCell = 0;
	for(int32_t Row = 0; Row < Grid.Height; ++Row)
	{
		for(int32_t Col = 0; Col < Grid.Width; ++Col)
		{
			struct cell *Cell = (Grid.Cells + ((Row * Grid.Width) + Col));
			if(Cell->Visited == 0)
			{
				int32_t Size = FloodFill(&Grid, Row, Col);
				if(Size > MaxSize)
				{
					MaxSize = Size;
					MaxCell = Cell;
				}
			}
		}
	}

	for(int32_t Row = 0; Row < Grid.Height; ++Row)
	{
		for(int32_t Col = 0; Col < Grid.Width; ++Col)
		{
			struct cell *Cell = (Grid.Cells + ((Row * Grid.Width) + Col));
			printf("%c", Cell->Value);
		}

		printf("\n");
	}

	printf("Size = %d\n", MaxSize);
	printf("(%d, %d): %c\n", MaxCell->X, MaxCell->Y, MaxCell->Value);

	DestroyFileResult(&File);
	FreeGrid(&Grid);
	return 0;
}
